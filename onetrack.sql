-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2015 at 12:42 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onetrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_api`
--

CREATE TABLE IF NOT EXISTS `t_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `api_url` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_api`
--

INSERT INTO `t_api` (`id`, `api_key`, `status`, `api_url`) VALUES
(1, 0, 1, 'testJson');

-- --------------------------------------------------------

--
-- Table structure for table `t_divition`
--

CREATE TABLE IF NOT EXISTS `t_divition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `id_div` int(11) NOT NULL,
  `division_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_divition`
--

INSERT INTO `t_divition` (`id`, `status`, `id_div`, `division_name`) VALUES
(1, 1, 100, 'Brahmanbaria'),
(2, 1, 101, 'Dhaka');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
